#!/usr/bin/env python3

import sys
import re
from typing import NamedTuple, Optional, List


ANOMALOUS_TIME_THRESHOLD_SECONDS = 30
OK_STATES = {"ok", "run", "down"}
OK_EXTRAS = {"paused", "normally up", "normally down"}

TERM_RED   = "\033[31m"
TERM_GREEN = "\033[32m"
TERM_YELLW = "\033[33m"
TERM_BOLD  = "\033[1m"
TERM_ITAL  = "\033[3m"
TERM_ULINE = "\033[4m"
TERM_RESET = "\033[0m"


class SvStatus(NamedTuple):
	name:  str
	state: str
	pid:   Optional[int]
	time:  Optional[int]
	extra: List[str]
	anomalous_state:  bool
	anomalous_time:   bool
	anomalous_extras: bool
	anomalous:        bool


def svstatus_parse(line):
	m = re.fullmatch(
		r'([^:]+): (?:[^: ]*/)?([^/: ]+)/?: (?:\(pid ([0-9]+)\) )?([0-9]+)s(?:, ([^;]+))?(?:; .+)?',
		line.rstrip()
	)
	if not m:
		m = re.fullmatch(
			r'([^:]+): (?:[^: ]*/)?([^/: ]+)/?: ()()(.+)',
			line.rstrip()
		)
	if not m:
		raise ValueError("Failed to parse line")
	state, name, pid, time, extra = m.groups()

	time = int(time) if time else None
	extra = extra.split(", ") if extra else []

	anomalous_state  = state not in OK_STATES
	anomalous_time   = time is not None and time < ANOMALOUS_TIME_THRESHOLD_SECONDS
	anomalous_extras = any(e not in OK_EXTRAS for e in extra)

	return SvStatus(
		name   = name,
		state  = state,
		pid    = int(pid) if pid else None,
		time   = time,
		extra  = extra,
		anomalous_state  = anomalous_state,
		anomalous_time   = anomalous_time,
		anomalous_extras = anomalous_extras,
		anomalous        = anomalous_state or anomalous_time or anomalous_extras
	)


def format_time(total_seconds):
	if total_seconds is None:
		return ""
	if total_seconds < 60:
		return f"{total_seconds}s"
	total_minutes, seconds = divmod(total_seconds, 60)
	if total_minutes < 60:
		return f"{total_minutes}m{seconds:02}s"
	total_hours, minutes = divmod(total_minutes, 60)
	if total_hours < 24:
		return f"{total_hours}h{minutes:02}m"
	total_days, hours = divmod(total_hours, 24)
	return f"{total_days}d{hours:02}h"


def svstatus_format(s, name_column_len):
	bullet = (
		f"{TERM_RED  }🞫{TERM_RESET}" if s.anomalous_state or s.anomalous_extras else
		f"{TERM_YELLW}●{TERM_RESET}" if s.state == "run" and s.anomalous_time else
		f"{TERM_GREEN}●{TERM_RESET}" if s.state == "run" else
		f"{TERM_GREEN}○{TERM_RESET}" if s.state == "down" else
		" "
	)
	extra = [
		e if e in OK_EXTRAS else f"{TERM_BOLD}{e}{TERM_RESET}"
		for e in s.extra
	]
	if s.state == "run" and s.anomalous_time:
		extra.append("recently started -- problem if not intentional")

	return (
		f"{s.name:>{name_column_len}s} "
		f"{bullet} {TERM_BOLD if s.anomalous_state else ''}{s.state:7s}{TERM_RESET}  "
		f"{TERM_BOLD if s.anomalous_time else ''}{format_time(s.time):>6s}{TERM_RESET}  "
		f"{str(s.pid if s.pid is not None else ''):6s}  "
		f"{', '.join(extra)}"
	)


def main():
	only_anomalous = False
	try:
		if sys.argv[1] in ["-h", "--help"]:
			print(f"""
sv-porcelain: make {TERM_ITAL}sv status{TERM_RESET} more readable at a glance
Usage: sv status $SVDIR/*/ | {sys.argv[0]} [-q]

Options:
  -q, --only-anomalous         Hide services where everything looks okay
  -h, --help                   Show this help text and exit

Symbols in output:
  {TERM_GREEN}○{TERM_RESET} down
  {TERM_GREEN}●{TERM_RESET} run
  {TERM_YELLW}●{TERM_RESET} run but not for long
  {TERM_RED  }🞫{TERM_RESET} something's off
			""".strip())
			return
		elif sys.argv[1] in ["-q", "--only-anomalous"]:
			only_anomalous = True
	except IndexError:
		pass

	svstatuses = []
	for line_no, line in enumerate(sys.stdin, start=1):
		try:
			svstatuses.append(svstatus_parse(line))
		except Exception as exc:
			raise Exception(f"Problem with line {line_no}: {line.rstrip()}") from exc

	svstatuses_to_print = [s for s in svstatuses if s.anomalous] if only_anomalous else svstatuses

	if svstatuses_to_print:
		name_column_len = max(
			10,
			max((len(s.name) for s in svstatuses_to_print), default=0)
		)

		# Print header
		print(f"{TERM_ULINE}{'Service':>{name_column_len}s}   State     Since  PID   {TERM_RESET}")

		for svstatus in svstatuses_to_print:
			print(svstatus_format(svstatus, name_column_len=name_column_len))


if __name__ == '__main__':
	main()
