# Status porcelain

This is a small process monitor that can work either with PID files or with [sv].

## [sv]-based
![Screenshot of a terminal showing the tool](./screenshot.png)

`sv-based/sv-porcelain.py` accepts the output of `sv status <svservice ...>` on stdin.

`sv-based/svstatus` is a wrapper that calls `sv-porcelain.py` with all entries in `$SVDIR`.

## PID-based
`pid-based/status.py` accepts filenames of PID files on stdin. The service name is taken from the PID file's filename, stripping `.pid` if present.

`pid-based/status` is a wrapper that calls `status.py` with all files in `$HOME/.pid`. (Customize to your liking.)

[sv]: http://smarden.org/runit/sv.8.html
